# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.1.4

- minor:  Added DESTROY flag to control lyfecycle of the instance.
          Added CREATE flag to control lyfecycle of the instance.

## 0.1.3

- minor: Added GCR authentication into the Compute Engine instance to avoid issues with a private registry

## 0.1.2

- minor:  Update COS version to cos-stable-81-12871-1160-0
          Add new variable SLEEP to override the default wait time

## 0.1.1

- minor: Add support to override ENTRYPOINT

## 0.1.0

- minor: Initial release of Bitbucket Pipelines Google Cloud Container execution in Compute Engine.

