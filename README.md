# Bitbucket Pipelines Pipe: Google Cloud Container Compute Engine One Time Execution

Pipe to execute a container into a compute engine instance. This create a temporary instance, execute and destroy the instance.

This pipe is based on [google-cloud-storage-deploy](https://bitbucket.org/atlassian/google-cloud-storage-deploy/src/master/) pipe, this is not an atlassian official pipe, but at this moment not exist a pipeline to deploy to Cloud Run.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: 0x365/google-cloud-container-ce:0.1.4
  variables:
    KEY_FILE: '<string>'
    PROJECT: '<string>'
    SERVICE_ACCOUNT: '<string>'
    DOCKER_IMAGE: '<string>'
    # DOCKER_TAG: '<string>' # Optional.
    # DOCKER_REGISTRY: '<string>' # Optional.
    # ZONE: '<string>' # Optional.
    # ENVIRONMENT: '<string>' # Optional.
    # MACHINE_TYPE: '<string>' # Optional.
    # SUBNET: '<string>' # Optional.
    # NETWORK_TIER: '<integer>' # Optional.
    # SCOPES: '<integer>' # Optional.
    # OS_IMAGE: '<integer>' # Optional.
    # GOOGLE_LOGGING_ENABLED: '<string>' # Optional.
    # SLEEP: '<integer>' # Optional.
    # DESTROY: '<boolean>' # Optional.
```

## Variables

| Variable                   | Usage                                                |
| ----------------------------- | ---------------------------------------------------- |
| KEY_FILE (*)                  |  base64 encoded Key file for a [Google service account](https://cloud.google.com/iam/docs/creating-managing-service-account-keys). |
| PROJECT (*)                   |  The project that owns the cloud run service to deploy into. |
| SERVICE_ACCOUNT (*)                    |  Google Cloud Run service account name. |
| DOCKER_IMAGE (*)                    |  The source of the docker image to deploy |
| DOCKER_TAG                     |  Image Tag to the docker image, Default: `latest` |
| DOCKER_REGISTRY                     |  Hostname is gcr.io, us.gcr.io, eu.gcr.io, or asia.gcr.io, Default: `us.gcr.io` |
| ZONE                     |  Options: https://cloud.google.com/compute/docs/regions-zones#available Default: `us-central1-a` |
| ENTRYPOINT                  | Override ENTRYPOINT |
| ENVIRONMENT                    |  Define environment variables. format: `VAR1=VALUE1,VAR2=VALUE2,VAR3=VALUE3` |
| MACHINE_TYPE                           |  Options: https://cloud.google.com/compute/docs/machine-types  Default: `f1-micro` |
| SUBNET                    |  Network subnet. Default: `default` |
| NETWORK_TIER                    |  Network Tier. Default: `PREMIUM` |
| SCOPES                         |  List of permission scopes, Default: `https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append` |
| OS_IMAGE                         |  https://cloud.google.com/container-optimized-os/docs/release-notes Default: `cos-stable-81-12871-181-0`. |
| GOOGLE_LOGGING_ENABLED                         |  Logs will be send to stackdriver. Default: `true`. |
| SLEEP                         |  Sleep time to wait to execute container after the machine creation. Default: `60`. |
| CREATE                         |  Create instance in this execution. Default: `true`. |
| DESTROY                         |  Destroy instance after the execution. Default: `true`. |

_(*) = required variable._

## Examples

### Basic example:

```yaml
script:
  - pipe: 0x365/google-cloud-container-ce:0.1.4
    variables:
      KEY_FILE: $KEY_FILE
      PROJECT: 'my-project'
      SERVICE_ACCOUNT: 'service_account@compute-engine.com'
      DOCKER_IMAGE: 'us.gcr.io/x365-pipes/tools/sleep:latest'
      USER: 'user'
      ENVIRONMENT: 'VAR1=VALUE1,VAR2=VALUE2'
```

### Advanced example: 
    
```yaml
script:
  - pipe: 0x365/google-cloud-container-ce:0.1.4
    variables:
      KEY_FILE: $KEY_FILE
      PROJECT: 'my-project'
      SERVICE_ACCOUNT: 'service_account@compute-engine.com'
      USER: 'user'
      DOCKER_IMAGE: 'us.gcr.io/x365-pipes/tools/sleep:latest'
      DOCKER_TAG: 'v5'
      ENVIRONMENT: 'VAR1=VALUE1,VAR2=VALUE2'
      ZONE: 'us-east1-b'
      MACHINE_TYPE: 'g1-small'
      SUBNET: 'mysubnet'
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, send me an email to alexander.moreno at 0x365.com or 

If you’re reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce


## License
Copyright (c) 2020 Alexander Moreno Delgado and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.


[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-pipelines-questions?add-tags=pipes,google
