#!/usr/bin/env sh

# Create a machine to execute a container one time using CE, https://cloud.google.com/container-options/
#
# Required globals:
#   KEY_FILE
#   PROJECT
#   SERVICE_ACCOUNT
#   USER_SSH
#   DOCKER_IMAGE
#
# Option globals
#   DOCKER_TAG: Default: latest
#   ZONE: Options: https://cloud.google.com/compute/docs/regions-zones#available Default: us-central1-a
#   ENVIRONMENT: Default: ''
#   MACHINE_TYPE: Options: https://cloud.google.com/compute/docs/machine-types  Default: f1-micro
#   SUBNET: Default: default
#   NETWORK_TIER: Default: PREMIUM
#   SCOPES
#   OS_IMAGE: https://cloud.google.com/container-optimized-os/docs/release-notes
#   GOOGLE_LOGGING_ENABLED

# Sample to run locally


source "$(dirname "$0")/common.sh"
enable_debug

# mandatory parameters

export

KEY_FILE=${KEY_FILE:?'KEY_FILE variable missing.'}
PROJECT=${PROJECT:?'PROJECT variable missing.'}
SERVICE_ACCOUNT=${SERVICE_ACCOUNT:?'SERVICE_ACCOUNT variable missing.'}
USER=${USER:?'USER variable missing.'}
DOCKER_IMAGE=${DOCKER_IMAGE:?'DOCKER_IMAGE variable missing.'}
CURRENT_DATE=`date +%Y%m%d-%H%M%S`
INSTANCE_NAME=${INSTANCE_NAME:-"ote-${CURRENT_DATE}"}

# optionals parameters
DOCKER_TAG=${DOCKER_TAG:-'latest'}
DOCKER_REGISTRY=${DOCKER_REGISTRY:-'us.gcr.io'}
ZONE=${ZONE:-'us-central1-a'}
ENTRYPOINT=${ENTRYPOINT:-''}
ENVIRONMENT=${ENVIRONMENT:-''}
MACHINE_TYPE=${MACHINE_TYPE:-'f1-micro'}
SUBNET=${SUBNET:-'default'}
NETWORK_TIER=${NETWORK_TIER:-'PREMIUM'}
SCOPES=${SCOPES:-'https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append'}
OS_IMAGE=${OS_IMAGE:-'cos-stable-81-12871-1160-0'}
GOOGLE_LOGGING_ENABLED=${GOOGLE_LOGGING_ENABLED:-'true'}
SLEEP=${SLEEP:-'60'}
CREATE=${CREATE:-'true'}
DESTROY=${DESTROY:-'true'}

info "Validating inputs variables"

if [ "$ENVIRONMENT" != "" ] 
  then
    ENV=""
    VARIABLES=`echo $ENVIRONMENT | tr "," "\n"`
    for variable in $VARIABLES
      do
        ENV="${ENV} -e \"${variable}\""
      done
  else
    ENV=""
fi

echo $ENV

if [ "$ENTRYPOINT" != "" ]
  then
    ENT="--entrypoint ${ENTRYPOINT}"
  else
    ENT=""
fi

echo $ENT

info "Setting up environment".

echo "${KEY_FILE}" | base64 -d >> /tmp/key-file.json
run gcloud auth activate-service-account --key-file /tmp/key-file.json --quiet ${gcloud_debug_args}
run gcloud config set project $PROJECT --quiet ${gcloud_debug_args}
run gcloud auth configure-docker --quiet

if [[ ${CREATE} == "true" ]]; then
  info "Building machine"
  run gcloud beta compute --project=${PROJECT} instances create ${INSTANCE_NAME} --zone=${ZONE} --machine-type=${MACHINE_TYPE} --subnet=${SUBNET} --network-tier=${NETWORK_TIER} --metadata=google-logging-enabled=${GOOGLE_LOGGING_ENABLED} --maintenance-policy=MIGRATE --service-account=${SERVICE_ACCOUNT} --scopes=${SCOPES} --image=${OS_IMAGE} --image-project=cos-cloud --boot-disk-size=10GB --boot-disk-type=pd-standard --boot-disk-device-name=${INSTANCE_NAME}
  info "Waiting for initial setup ..."
  run sleep ${SLEEP}
fi

info  "Authenticate with GCR into the Compute Instance"

gcloud compute scp --zone=${ZONE} /tmp/key-file.json ${USER}@${INSTANCE_NAME}:/tmp/key-file.json
gcloud compute ssh ${USER}@${INSTANCE_NAME} --project ${PROJECT} --zone ${ZONE} --quiet --command "cat /tmp/key-file.json | docker login -u _json_key --password-stdin https://${DOCKER_REGISTRY}"

info "Executing container"

gcloud compute ssh ${USER}@${INSTANCE_NAME} --project ${PROJECT} --zone ${ZONE} --quiet --command "docker run ${ENV} ${ENT} ${DOCKER_IMAGE}:${DOCKER_TAG}"
status=$?

if [[ ${DESTROY} == "true" ]]; then
  info "Destroying machine"
  run gcloud compute instances delete ${INSTANCE_NAME} --project ${PROJECT} --zone ${ZONE} --quiet
fi

if [ "${status}" -eq 0 ]; then
  success "Execution successful."
else
  fail "Execution failed."
fi