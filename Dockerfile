FROM google/cloud-sdk:alpine

RUN gcloud components install beta --quiet
COPY pipe /usr/bin/
RUN chmod +x /usr/bin/pipe.sh

ENTRYPOINT ["/usr/bin/pipe.sh"]